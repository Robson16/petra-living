<?php

/**
 * Página inicial estática
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */
get_header();
?>

<main>
	<header class="jumbotron rounded-0 m-0" style="background-image: url(<?php echo get_template_directory_uri() . '/images/fachada-petra-living.jpg'; ?>);">
		<div class="container">
			<div class="row">
				<div class="col-8 col-lg-5 offset-4 offset-lg-7">
					<h1 class="display-4 display-sm-4 text-uppercase text-right text-md-left">Petra Living</h1>
					<h2 class="display-4 display-sm-4 text-white text-right text-md-left mb-5">inevitavelmente<br><span class="font-weight-bold">seu</span></h2>
					<a class="btn btn-black float-right float-md-none font-weight-light rounded-0 mt-lg-5 anchor" href="#sobre" role="button">
						Venha <span class="font-weight-bold">Conhecer</span>
					</a>
				</div>
				<!--/.col-->
			</div>
			<!--/.row-->
		</div>
		<!--/.container-->
	</header>

	<section id="sobre">
		<div class="container py-5">
			<div class="row">
				<div class="col-12 col-lg-6">
					<img class="img-fluid mb-4 mb-lg-0" width="640" height="480" src="<?php echo get_template_directory_uri() . '/images/portaria-petra-living.jpg'; ?>" alt="Portaria - Petra Living">
				</div>
				<!--/.col-->
				<div class="col-12 col-lg-3">
					<img class="img-fluid mb-4" width="255" height="191" src="<?php echo get_template_directory_uri() . '/images/sala-petra-living.jpg'; ?>" alt="Sala - Petra Living">
					<img class="img-fluid mb-4 mb-lg-0" width="255" height="191" src="<?php echo get_template_directory_uri() . '/images/cozinha-petra-living.jpg'; ?>" alt="Cozinha - Petra Living">
				</div>
				<!--/.col-->
				<div class="col-12 col-lg-3">
					<p class="display-1 font-weight-bold m-0 text-center" style="line-height: 0.8;">
						91m2
					</p>
					<hr class="border-dark">
					<p class="d-flex flex-column flex-lg-row m-0 text-center">
						<span class="display-2 font-weight-bold" style="line-height: 0.8;">3</span>
						<span class="font-weight-bold h3 m-0">dormitórios com 1 suíte</span>
					</p>
					<hr class="border-dark">
					<h4 class="my-3 text-center">
						lazer <span class="font-weight-bold">completo</span>
						<br>
						piscina, playground, espaço pet, festa e fitness.
					</h4>
					<a class="btn btn-black btn-block btn-lg float-md-none rounded-0 font-weight-light text-center anchor" href="#contato" role="button">
						Entre em <span class="font-weight-bold">Contato</span>
					</a>
				</div>
				<!--/.col-->
			</div>
			<!--/.row-->
			<div class="row">
				<div class="col-12">
					<hr class="border-dark">
					<h3 class="display-4 display-sm-4 text-center"><span class="font-weight-bold">inevitavelmente</span> os melhores serviços</h3>
					<hr class="border-dark">
				</div>
				<!--/.col-->
			</div>
			<!--/.row-->
			<div class="row">
				<div class="col-6 col-md-4 col-lg-2">
					<div class="position-relative d-flex justify-content-center mt-3 py-3" style="background-color: #f7a800;">
						<i class="position-absolute icon-box" style="top: 50%; transform: translateY(-50%);" aria-label="Smart Delivery"></i>
						<h5 class="text-white text-uppercase m-0" style="z-index: 10;">Smart<br><span class="font-weight-bold">Delivery</span></h5>
					</div>
				</div>
				<!--/.col-->
				<div class="col-6 col-md-4 col-lg-2">
					<div class="position-relative d-flex justify-content-center mt-3 py-3" style="background-color: #64470b;">
						<i class="position-absolute icon-chef" style="top: 50%; transform: translateY(-50%);" aria-label="Kitchen Experience"></i>
						<h5 class="text-white text-uppercase m-0" style="z-index: 10;">Kitchen<br><span class="font-weight-bold">Experience</span></h5>
					</div>
				</div>
				<!--/.col-->
				<div class="col-6 col-md-4 col-lg-2">
					<div class="position-relative d-flex justify-content-center mt-3 py-3" style="background-color: #979696;">
						<i class="position-absolute icon-work-station" style="top: 50%; transform: translateY(-50%);" aria-label="Work Place"></i>
						<h5 class="text-white text-uppercase m-0" style="z-index: 10;">Work<br><span class="font-weight-bold">Place</span></h5>
					</div>
				</div>
				<!--/.col-->
				<div class="col-6 col-md-4 col-lg-2">
					<div class="position-relative d-flex justify-content-center mt-3 py-3" style="background-color: #cac9c7;">
						<i class="position-absolute icon-dog" style="top: 50%; transform: translateY(-50%);" aria-label="Pet Care"></i>
						<h5 class="text-white text-uppercase m-0" style="z-index: 10;">Pet<br><span class="font-weight-bold">Care</span></h5>
					</div>
				</div>
				<!--/.col-->
				<div class="col-6 col-md-4 col-lg-2">
					<div class="position-relative d-flex justify-content-center mt-3 py-3" style="background-color: #2e2207;">
						<i class="position-absolute icon-dumbbell" style="top: 50%; transform: translateY(-50%);" aria-label="My Training"></i>
						<h5 class="text-white text-uppercase m-0" style="z-index: 10;">My<br><span class="font-weight-bold">Training</span></h5>
					</div>
				</div>
				<!--/.col-->
				<div class="col-6 col-md-4 col-lg-2">
					<div class="position-relative d-flex justify-content-center mt-3 py-3" style="background-color: #a2afc0;">
						<i class="position-absolute icon-harvest" style="top: 50%; transform: translateY(-50%);" aria-label="Nossa Horta"></i>
						<h5 class="text-white text-uppercase m-0" style="z-index: 10;">Nossa<br><span class="font-weight-bold">Horta</span></h5>
					</div>
				</div>
				<!--/.col-->
			</div>
			<!--/.row-->
		</div>
		<!--/.container-->
	</section>

	<section id="perspectivas">
		<div class="container-fluid pt-5">
			<div class="row">
				<div class="col-12">
					<h3 class="display-4 display-sm-4 text-uppercase text-center mb-3">Perspectivas</h3>
				</div>
				<!--/.col-->

				<?php
				$images = rwmb_meta('_petra_gallery', array(
					'size' => 'mini_perspective',
				));
				?>

				<?php foreach ($images as $image) : ?>
					<a href="<?php echo $image['full_url']; ?>" data-toggle="lightbox" data-gallery="example-gallery" class="col-6 col-md-4 col-lg-2 mb-3" aria-label="<?php echo ($image['alt']) ? $image['alt'] : $image['title']; ?>
                       ">
						<img src="<?php echo $image['url']; ?>" width="<?php echo $image['width']; ?>" height="<?php echo $image['height']; ?>" class="img-fluid" alt="<?php echo ($image['alt']) ? $image['alt'] : $image['title']; ?>">
					</a>
				<?php endforeach; ?>

			</div>
			<!--/.row-->
		</div>
		<!--/.container-->
	</section>

	<section id="plantas">
		<div class="container py-5">
			<h3 class="display-4 display-sm-4 text-uppercase text-center mb-3">Plantas</h3>
			<div class="row">
				<div class="col-12 col-md-6">
					<div class="d-flex flex-column align-items-center bg-light my-3">
						<img class="img-fluid p-3" width="500" height="563" src="<?php echo get_template_directory_uri() . '/images/petra-living-planta-a.png'; ?>" alt="Petra Living Planta Carrara">
						<div class="d-flex flex-column flex-lg-row align-items-center align-items-center">
							<div class="m-3">
								<p class="display-3 font-weight-bold text-center m-0">
									90,59m2
								</p>
								<p class="text-right m-0">
									+ Depósito Privativo
								</p>
							</div>
							<div class="border-left border-dark my-3 px-3">
								<h5 class="text-center font-weight-bold">Planta Carrara</h5>
								<a class="btn btn-black btn-block btn-md rounded-0 font-weight-light text-center anchor" href="#contato" role="button">
									Reserve o <span class="font-weight-bold">Seu Aqui</span>
								</a>
							</div>
						</div>
					</div>
				</div>
				<!--/.col-->
				<div class="col-12 col-md-6">
					<div class="d-flex flex-column align-items-center bg-light my-3">
						<img class="img-fluid p-3" width="500" height="563" src="<?php echo get_template_directory_uri() . '/images/petra-living-planta-b.png'; ?>" alt="Petra Living Planta Travertino">
						<div class="d-flex flex-column flex-lg-row align-items-center">
							<div class="m-3">
								<p class="display-3 font-weight-bold text-center m-0">
									91,25m2
								</p>
								<p class="text-right m-0">
									+ Depósito Privativo
								</p>
							</div>
							<div class="border-left border-dark my-3 px-3">
								<h5 class="text-center font-weight-bold">Planta Travertino</h5>
								<a class="btn btn-black btn-block btn-md rounded-0 font-weight-light text-center anchor" href="#contato" role="button">
									Reserve o <span class="font-weight-bold">Seu Aqui</span>
								</a>
							</div>
						</div>
					</div>
				</div>
				<!--/.col-->
			</div>
			<!--/.row-->
		</div>
		<!--/.container-->
	</section>

	<section id="localizacao">
		<div class="container pt-5">
			<h3 class="display-4 display-sm-4 text-uppercase text-center my-3">Localização</h3>
		</div>
		<div id="map"></div>
		<div class="container d-flex flex-column flex-lg-row justify-content-around align-items-center py-5">
			<p id="address" class="h4 text-uppercase text-color-one text-center text-lg-right my-3 my-lg-0 ">Rua Augusto Lippel - Parque Campolim, Sorocaba - SP, CEP: 18048-130</p>
			<a class="btn btn-black rounded-0 text-uppercase text-white py-3 px-5" href="//www.google.com/maps/?q=-23.536764,-47.467754" target="_blank" rel="noopener"><span class="font-weight-bold">Traçar rota</span> no Google Maps</a>
		</div>
		<script>
			function initMap() {
				var coordinates = {
					lat: -23.536764,
					lng: -47.467754
				};
				var map = new google.maps.Map(document.getElementById('map'), {
					zoom: 18,
					center: coordinates
				});
				var marker = new google.maps.Marker({
					position: coordinates,
					map: map
				});
			}
		</script>
		<script async defer src="//maps.googleapis.com/maps/api/js?key=<?php echo get_theme_mod('set_map_apikey'); ?>&callback=initMap"></script>
	</section>

	<section id="contato" class="position-relative">
		<div class="container py-5">
			<h3 class="display-4 display-sm-4 text-uppercase text-center mb-3">Contato</h3>
			<div class="row">
				<div class="col-12 col-lg-4">
					<p class="h3 text-center text-lg-right font-weight-light"><span class="font-weight-bold">Entre em contato</span> através do <span class="font-weight-bold">formulário</span> e um dos <span class="font-weight-bold">nossos corretores de plantão</span> irá te atender!</p>
				</div>
				<div class="col-12 col-lg-4">
					<?php echo do_shortcode(get_theme_mod('set_contact_form')); ?>
				</div>
				<div class="col-12 col-lg-4">
					<p class="h3 text-center text-lg-left font-weight-light">Atendimento através do telefone:</p>
					<ul class="list-unstyled text-center text-lg-left">
						<?php if (get_theme_mod('set_contact_phone_one')) : ?>
							<li>
								<a class="h3 font-weight-bold" href="tel:<?php echo clear_phones(get_theme_mod('set_contact_phone_one')); ?>">
									<?php echo get_theme_mod('set_contact_phone_one'); ?>
								</a>
							</li>
						<?php endif; ?>
						<?php if (get_theme_mod('set_contact_phone_two')) : ?>
							<li>
								<a class="h3 font-weight-bold" href="tel:<?php echo clear_phones(get_theme_mod('set_contact_phone_two')); ?>">
									<?php echo get_theme_mod('set_contact_phone_two'); ?>
								</a>
							</li>
						<?php endif; ?>
					</ul>
				</div>
			</div>
		</div>
	</section>

</main>

<?php
get_footer();
