<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php wp_body_open(); ?>

	<nav id="main-menu" class="navbar navbar-expand-lg navbar-light">
		<div class="container">
			<a class="navbar-brand" href="<?php echo get_home_url(); ?>" aria-label="Petra Living">
				<?php get_template_part('template-parts/logos/logo', 'petra-living'); ?>
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item">
						<a class="nav-link anchor" href="#sobre">Conheça</a>
					</li>
					<li class="nav-item">
						<a class="nav-link anchor" href="#perspectivas">Perspectivas</a>
					</li>
					<li class="nav-item">
						<a class="nav-link anchor" href="#plantas">Plantas</a>
					</li>
					<li class="nav-item">
						<a class="nav-link anchor" href="#localizacao">Localização</a>
					</li>
					<li class="nav-item">
						<a class="nav-link anchor" href="#contato">Contato</a>
					</li>
				</ul>
			</div>
		</div>
		<!--/.container-->
	</nav>