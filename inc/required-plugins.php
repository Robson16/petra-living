<?php

/**
 * @see http://tgmpluginactivation.com/configuration/ for detailed documentation.
 *
 * @package    TGM-Plugin-Activation
 * @subpackage Example
 * @version    2.6.1 for parent theme Petra Land-Page
 * @author     Thomas Griffin, Gary Jones, Juliette Reinders Folmer
 * @copyright  Copyright (c) 2011, Thomas Griffin
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       https://github.com/TGMPA/TGM-Plugin-Activation
 */
require_once get_template_directory() . '/classes/class-tgm-plugin-activation.php';

add_action('tgmpa_register', 'petra_register_required_plugins');

function petra_register_required_plugins() {
    $plugins = array(
        // Inclua um plug-in no repositório de plug-ins do WordPress.
        array(
            'name' => 'Contact Form 7',
            'slug' => 'contact-form-7',
            'required' => true,
        ),
        array(
            'name' => 'Flamingo',
            'slug' => 'flamingo',
            'required' => true,
        ),
        array(
            'name' => 'Meta Box – WordPress Custom Fields Framework',
            'slug' => 'meta-box',
            'required' => true,
        ),
        array(
            'name' => 'Editor clássico',
            'slug' => 'classic-editor',
            'required' => true,
        ),
    );

    $config = array(
        'id' => 'petra', // Unique ID for hashing notices for multiple instances of TGMPA.
        'default_path' => '', // Default absolute path to bundled plugins.
        'menu' => 'tgmpa-install-plugins', // Menu slug.
        'has_notices' => true, // Show admin notices or not.
        'dismissable' => true, // If false, a user cannot dismiss the nag message.
        'dismiss_msg' => '', // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => false, // Automatically activate plugins after installation or not.
        'message' => '', // Message to output right before the plugins table.
        'strings' => array(
            'page_title' => __('Install Required Plugins', 'petra'),
            'menu_title' => __('Install Plugins', 'petra'),
            'installing' => __('Installing Plugin: %s', 'petra'),
            'updating' => __('Updating Plugin: %s', 'petra'),
            'oops' => __('Something went wrong with the plugin API.', 'petra'),
            'notice_can_install_required' => _n_noop('This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.', 'petra'),
            'notice_can_install_recommended' => _n_noop('This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.', 'petra'),
            'notice_ask_to_update' => _n_noop('The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.', 'petra'),
            'notice_ask_to_update_maybe' => _n_noop('There is an update available for: %1$s.', 'There are updates available for the following plugins: %1$s.', 'petra'),
            'notice_can_activate_required' => _n_noop('The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.', 'petra'),
            'notice_can_activate_recommended' => _n_noop('The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.', 'petra'),
            'install_link' => _n_noop('Begin installing plugin', 'Begin installing plugins', 'petra'),
            'update_link' => _n_noop('Begin updating plugin', 'Begin updating plugins', 'petra'),
            'activate_link' => _n_noop('Begin activating plugin', 'Begin activating plugins', 'petra'),
            'return' => __('Return to Required Plugins Installer', 'petra'),
            'plugin_activated' => __('Plugin activated successfully.', 'petra'),
            'activated_successfully' => __('The following plugin was activated successfully:', 'petra'),
            'plugin_already_active' => __('No action taken. Plugin %1$s was already active.', 'petra'),
            'plugin_needs_higher_version' => __('Plugin not activated. A higher version of %s is needed for this theme. Please update the plugin.', 'petra'),
            'complete' => __('All plugins installed and activated successfully. %1$s', 'petra'),
            'dismiss' => __('Dismiss this notice', 'petra'),
            'notice_cannot_install_activate' => __('There are one or more required or recommended plugins to install, update or activate.', 'petra'),
            'contact_admin' => __('Please contact the administrator of this site for help.', 'petra'),
            'nag_type' => '', // Determines admin notice type - can only be one of the typical WP notice classes, such as 'updated', 'update-nag', 'notice-warning', 'notice-info' or 'error'. Some of which may not work as expected in older WP versions.
        ),
    );

    tgmpa($plugins, $config);
}
