<?php

/**
 * Petra Land-Page: Customizer
 *
 */
function petra_customize_register($wp_customize) {
    
    // Redes Sociais
    $wp_customize->add_section('sec_social_networks', array(
        'title' => esc_html__('Redes Sociais', 'petra'),
        'description' => esc_html__('Links das páginas de rede sociais', 'petra'),
        'priority' => 150
    ));

    // Redes Sociais - Instagram
    $wp_customize->add_setting('set_instagram', array(
        'sanitize_callback' => 'esc_url_raw' //cleans URL from all invalid characters
    ));
    $wp_customize->add_control('set_instagram', array(
        'label' => esc_html__('Instagram', 'theme_slug'),
        'section' => 'sec_social_networks',
        'type' => 'url'
    ));

    // Redes Sociais - Facebook
    $wp_customize->add_setting('set_facebook', array(
        'sanitize_callback' => 'esc_url_raw' //cleans URL from all invalid characters
    ));
    $wp_customize->add_control('set_facebook', array(
        'label' => esc_html__('Facebook', 'theme_slug'),
        'section' => 'sec_social_networks',
        'type' => 'url'
    ));

    // Contato
    $wp_customize->add_section('sec_contact', array(
        'title' => esc_html__('Contato', 'petra'),
        'priority' => 150
    ));

    // Contato - Formulário
    $wp_customize->add_setting('set_contact_form', array(
        'type' => 'theme_mod',
        'sanitize_callback' => 'wp_kses_post' //keeps only HTML tags that are allowed in post content
    ));
    $wp_customize->add_control('set_contact_form', array(
        'label' => esc_html__('Formulário', 'petra'),
        'description' => esc_html__('Shortcode do Contact Form 7', 'petra'),
        'section' => 'sec_contact',
        'type' => 'text'
    ));

    // Contato - Telefones
    $wp_customize->add_setting('set_contact_phone_one', array(
        'type' => 'theme_mod',
        'sanitize_callback' => 'wp_kses_post' //keeps only HTML tags that are allowed in post content
    ));
    $wp_customize->add_control('set_contact_phone_one', array(
        'label' => esc_html__('Telefone Um', 'petra'),
        'section' => 'sec_contact',
        'type' => 'text'
    ));
    
    $wp_customize->add_setting('set_contact_phone_two', array(
        'type' => 'theme_mod',
        'sanitize_callback' => 'wp_kses_post' //keeps only HTML tags that are allowed in post content
    ));
    $wp_customize->add_control('set_contact_phone_two', array(
        'label' => esc_html__('Telefone Dois', 'petra'),
        'section' => 'sec_contact',
        'type' => 'text'
    ));

    // Google Maps
    $wp_customize->add_section('sec_map', array(
        'title' => 'Mapa',
        'description' => 'Google Maps'
    ));

    // Google API Key
    $wp_customize->add_setting('set_map_apikey', array(
        'type' => 'theme_mod',
        'default' => '',
        'sanitize_callback' => 'wp_filter_nohtml_kses'
    ));
    $wp_customize->add_control('set_map_apikey', array(
        'label' => __('Chave da API', 'petra'),
        'description' => __('Cole a chave de API do Google Maps', 'petra'),
        'section' => 'sec_map',
        'type' => 'text'
    ));
}

add_action('customize_register', 'petra_customize_register');
