<?php

function petra_meta_box($meta_boxes) {
    $prefix = '_petra_';

    $meta_boxes[] = array(
        'id' => 'gallery',
        'title' => esc_html__('Galeria de Perspectivas', 'petra'),
        'post_types' => array('page'),
        'context' => 'after_title',
        'priority' => 'default',
        'autosave' => 'true',
        'fields' => array(
            array(
                'id' => $prefix . 'gallery',
                'type' => 'image_advanced',
                'name' => esc_html__('Imagens', 'petra'),
            ),
        ),
    );

    return $meta_boxes;
}

add_filter('rwmb_meta_boxes', 'petra_meta_box');
