<?php

/**
 * Funções que aprimoram o tema conectando-se ao WordPress
 *
 */

/**
 * Recebe um numero de telefone, e retorna-o sem qualquer outro caractere que não seja numerico
 * @param str $phone
 * @return str 
 */
function clear_phones($phone) {
    $chars = array(".", "(", ")", "-", " ");
    return str_replace($chars, "", $phone);
}
