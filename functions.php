<?php

/**
 * Petra Land-Page funções e definições
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 */

/**
 * Enfileiramento de scripts e styles.
 */
function petra_scripts()
{
	// CSS
	wp_enqueue_style('googlefonts', '//fonts.googleapis.com/css?family=Open+Sans:300,400,700', null, '1.0', 'all');
	wp_enqueue_style('bootstrap', '//stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css', null, '4.3.1', 'all');
	wp_enqueue_style('ekko-lightbox', '//cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css', array('bootstrap'), '5.3.0', 'all');
	wp_enqueue_style('petra-style', get_stylesheet_uri(), array('bootstrap'), wp_get_theme()->get('Version'));

	// Js
	wp_deregister_script('jquery');
	wp_register_script('jquery', '//code.jquery.com/jquery-3.4.1.min.js', array(), '3.4.1', true);
	wp_enqueue_script('jquery');
	wp_enqueue_script('bootstrap', '//stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js', array('jquery'), '4.3.1', true);
	wp_enqueue_script('ekko-lightbox', '//cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js', array('jquery', 'bootstrap'), '5.3.0', true);
}

add_action('wp_enqueue_scripts', 'petra_scripts');

/**
 * Configura os padrões do tema e registra o suporte para vários recursos do WordPress.
 */
if (!function_exists('petra_setup')) {

	function petra_setup()
	{
		// Deixe o WordPress gerenciar o título do documento.
		add_theme_support('title-tag');
		//
		add_image_size('mini_perspective', 300, 300, true);
	}
}

add_action('after_setup_theme', 'petra_setup');

/**
 *  TGM Plugin Configurações
 */
require_once get_template_directory() . '/inc/required-plugins.php';

/**
 * Funções, filtros e ações extras para o tema
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 *  Adições do personalizador.
 */
require_once get_template_directory() . '/inc/customizer.php';

/**
 *  Meta-Boxes
 */
require_once get_template_directory() . '/inc/meta-boxes.php';
