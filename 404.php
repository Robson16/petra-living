<?php

/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 */
get_header();
?>

<main>
	<section class="container py-5">
		<h1 class="page-title"><?php _e('Não é possível encontrar essa página', 'petra'); ?></h1>
		<p><?php _e('Parece que nada foi encontrado neste local', 'petra'); ?></p>
	</section>
</main>

<?php
get_footer();
