<?php

/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */
get_header();
?>

<main>
	<div class="container py-5">
		<?php if (have_posts()) : ?>
			<?php while (have_posts()) : the_post(); ?>
				<?php get_template_part('template-parts/content/content'); ?>
			<?php endwhile; ?>
			<div class="d-flex justify-content-between w-100">
				<div class="nav-previous"><?php previous_posts_link(__('Posts mais antigos', 'petra')); ?></div>
				<div class="nav-next"><?php next_posts_link(__('Posts mais recentes', 'petra')); ?></div>
			</div>
		<?php else : ?>
			<?php get_template_part('template-parts/content/content', 'none'); ?>
		<?php endif; ?>
	</div>
	<!--/.container-->
</main>

<?php
get_footer();
