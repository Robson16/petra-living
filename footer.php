<?php

/**
 * The template for displaying the footer
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 */
?>

<footer id="footer" style="background-image: url(<?php echo get_template_directory_uri() . '/images/bg-footer.jpg'; ?>);">
	<div class="container py-3">
		<div class="row">
			<div class="col-12 col-md-4 col-lg-2 d-flex justify-content-center align-items-center py-3">
				<?php get_template_part('template-parts/logos/logo', 'petra-living'); ?>
			</div>
			<div class="col-12 col-md-5 col-lg-3 offset-md-3 offset-lg-7 d-flex flex-column justify-content-center align-items-center align-items-md-end py-3">
				<ul class="list-inline">
					<?php if (get_theme_mod('set_facebook')) : ?>
						<li class="list-inline-item">
							<a href="<?php echo get_theme_mod('set_facebook'); ?>" target="_blank" rel="noopener" aria-label="Facebook Petra Living">
								<i class="icon-facebook"></i>
							</a>
						</li>
					<?php endif; ?>
					<?php if (get_theme_mod('set_instagram')) : ?>
						<li class="list-inline-item">
							<a href="<?php echo get_theme_mod('set_instagram'); ?>" target="_blank" rel="noopener" aria-label="Instagram Petra Living">
								<i class="icon-instagram"></i>
							</a>
						</li>
					<?php endif; ?>
				</ul>
				<a href="//www.construtorasilvacampos.com.br/" target="_blank" rel="noopener" aria-label="Construtora Silva Campos">
					<img class="img-fluid" width="300" height="80" src="<?php echo get_template_directory_uri() . '/images/logo-silvacampos.png'; ?>" alt="Construtora Silva Campos">
				</a>
			</div>
		</div>
		<p class="text-center text-white my-3">&copy; Petra Living, 2019 - Todos os direitos reservados</p>
	</div>
</footer>

<?php wp_footer(); ?>

<script>
	// Código para animar a barra de menu
	jQuery(document).ready(function($) {
		$(window).scroll(function() {
			var scroll = $(window).scrollTop();
			var viewportY = $(window).height();
			if (scroll > viewportY * 0.33) {
				$('#main-menu').addClass('fixed-top navbar-dark bg-black');
				$('.navbar-brand .logo-petra-living').addClass('is-small is-white');
			} else {
				$('#main-menu').removeClass('fixed-top navbar-dark bg-black');
				$('.navbar-brand .logo-petra-living').removeClass('is-small is-white');
			}
		});

		$(".anchor").click(function(event) {
			event.preventDefault();
			var idElemento = $(this).attr("href");
			var deslocamentoA = $(idElemento).offset().top;
			var deslocamento = deslocamentoA - $('#main-menu').outerHeight();
			$('html, body').animate({
				scrollTop: deslocamento
			}, 'slow');
		});

		$('a[data-toggle="lightbox"]').click(function(e) {
			e.preventDefault();
			$(this).ekkoLightbox();
		});
	});
</script>

</body>

</html>